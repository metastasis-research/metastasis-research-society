CONTENTS OF THIS FILE
---------------------

 * About Drupal
 * Configuration and features
 * Installation profiles
 * Appearance
 * Developing for Drupal

ABOUT DRUPAL
------------

Drupal is an open source content management platform supporting a variety of
websites ranging from personal weblogs to large community-driven websites. For
more information, see the Drupal website at http://drupal.org/, and join the
Drupal community at http://drupal.org/community.

Legal information about Drupal:
 * Know your rights when using Drupal:
   See LICENSE.txt in the same directory as this document.
 * Learn about the Drupal trademark and logo policy:
   http://drupal.com/trademark

CONFIGURATION AND FEATURES
--------------------------

Drupal core (what you get when you download and extract a drupal-x.y.tar.gz or
drupal-x.y.zip file from http://drupal.org/project/drupal) has what you need to
get started with your website. It includes several modules (extensions that add
functionality) for common website features, such as managing content, user
accounts, image uploading, and search. Core comes with many options that allow
site-specific configuration. In addition to the core modules, there are
thousands of contributed modules (for functionality not included with Drupal
core) available for download.

More about configuration:
 * Install, upgrade, and maintain Drupal:
   See INSTALL.txt and UPGRADE.txt in the same directory as this document.
 * Learn about how to use Drupal to create your site:
   http://drupal.org/documentation
 * Download contributed modules to sites/all/modules to extend Drupal's
   functionality:
   http://drupal.org/project/modules
 * See also: "Developing for Drupal" for writing your own modules, below.

INSTALLATION PROFILES
---------------------

Installation profiles define additional steps (such as enabling modules,
defining content types, etc.) that run after the base installation provided
by core when Drupal is first installed. There are two basic installation
profiles provided with Drupal core.

Installation profiles from the Drupal community modify the installation process
to provide a website for a specific use case, such as a CMS for media
publishers, a web-based project tracking tool, or a full-fledged CRM for
non-profit organizations raising money and accepting donations. They can be
distributed as bare installation profiles or as "distributions". Distributions
include Drupal core, the installation profile, and all other required
extensions, such as contributed and custom modules, themes, and third-party
libraries. Bare installation profiles require you to download Drupal Core and
the required extensions separately; place the downloaded profile in the
/profiles directory before you start the installation process. Note that the
contents of this directory may be overwritten during updates of Drupal core;
it is advised to keep code backups or use a version control system.

Additionally, modules and themes may be placed inside subdirectories in a
specific installation profile such as profiles/your_site_profile/modules and
profiles/your_site_profile/themes respectively to restrict their usage to only
sites that were installed with that specific profile.

More about installation profiles and distributions:
* Read about the difference between installation profiles and distributions:
  http://drupal.org/node/1089736
* Download contributed installation profiles and distributions:
  http://drupal.org/project/distributions
* Develop your own installation profile or distribution:
  http://drupal.org/developing/distributions

APPEARANCE
----------

In Drupal, the appearance of your site is set by the theme (themes are
extensions that set fonts, colors, and layout). Drupal core comes with several
themes. More themes are available for download, and you can also create your own
custom theme.

More about themes:
 * Download contributed themes to sites/all/themes to modify Drupal's
   appearance:
   http://drupal.org/project/themes
 * Develop your own theme:
   http://drupal.org/documentation/theme

DEVELOPING FOR DRUPAL
---------------------

Drupal contains an extensive API that allows you to add to and modify the
functionality of your site. The API consists of "hooks", which allow modules to
react to system events and customize Drupal's behavior, and functions that
standardize common operations such as database queries and form generation. The
flexible hook architecture means that you should never need to directly modify
the files that come with Drupal core to achieve the functionality you want;
instead, functionality modifications take the form of modules.

When you need new functionality for your Drupal site, search for existing
contributed modules. If you find a module that matches except for a bug or an
additional needed feature, change the module and contribute your improvements
back to the project in the form of a "patch". Create new custom modules only
when nothing existing comes close to what you need.

More about developing:
 * Search for existing contributed modules:
   http://drupal.org/project/modules
 * Contribute a patch:
   http://drupal.org/patch/submit
 * Develop your own module:
   http://drupal.org/developing/modules
 * Follow best practices:
   http://drupal.org/best-practices
 * Refer to the API documentation:
   http://api.drupal.org/api/drupal/7


Release 6/01/2017
Metastasis Research Society Bug and Enhancement Release


#5262017-1000 Membership & Renewals button at the top should have "and" instead of "&" which is apparently less confusing to international folk.  I was trying to save space. Replaced.

#5262017-1001 https://www.metastasis-research.com/news came through with a ‘not secure” warning screen in Firefox. Fix, wrong url inserted, changed to .org/news


#5262017-1002 Minor edit to the text on the donation slider shown in blue here.  We replaced the word "decent" with "optimal" and I have changed it on our mission page (sorry I know this is html): Replaced

 Our vision is to de-throne metastasis as a disease that steals lives by obtaining research results that translate to patients having an optimal quality of life while managing metastasis as a chronic condition, with the ultimate goal of achieving a cure.

#5262017-1003 All agree the upcoming conferences slide needs to be more readable.  I've attached what should be a much darker image without the presentation screen in it that makes the text disappear.  I really hope this one works since it is such a pain to change. Replaced with new slide


#5262017-1004 https://www.metastasis-research.org/basic-page/membership
This page when pulled up on a cell phone looks a bit crazy with the category listing becoming moved around.  I'm not sure what can be done about that.  It is a lot of text for a phone here. Waiting for format, not fixed yet. Excluded from this release, still waiting for fomat.

#5312017-1005  The become an MRS member photo at the bottom left side of the homepage is a bit blurry and the commenter said the members represented were about to retire.  So...I have found a different image to use.  Could you tell me what size this image needs to be and I'll send it over in the proper format for you? Replaced with new image 1000px x 425px in size. Replaced

#5312017-1006 The MRS is affiliated with 3 journals, but our official journal (most important one) is Clinical and Experimental Metastasis. The board member (who happens to be the editor and chief of Clinical and Experimental Metastasis) is concerned about the rotating journals at the bottom right of the homepage under "Journals".  He said when he logs in it looks like whatever journal is shown there is our journal when we should focus on promoting clinical and experimental metastasis.  Is there any way to make that lower right journal image under "Journals" not rotate out and keep the journal represented to Clinical and Experimental Metastasis?  It's OK to use the term "Journals" above. Changed the view to only show the 'Clinical and Experimental Metastasis' journal on front page.

#5312017-1008 Updated Rules email templates. Overwrote, E-mail for granted roles, E-mail for revoked roles, E-mail for role renewal and E-mail for role expiration reminders.

#5312017-1008 Turned on CSS and JavaScript Aggregation to speed up site. Not caching pages for anonymous users yet, want to see how this performs without that caching being set.

#6012017-1009 Removal of menu item "Meet The Experts" from Members Only Menu.

#6012017-1010 Change the page Recorded Webinars to Members Only Access.

Release 7/14/2017
Metastasis Research Society Bug and Enhancement Release


#5262017-1011 Enhancement - Changes to the rules which regulate the automated email responses. Please see associated word document Automated Website Emails 5-2017-1

#5262017-1012 Bug - The link to the journal Angiogenesis under the Scientist/Clinician tab doesn't seem to be working due to a security issue (the s in https). https://www.metastasis-research.com/angiogenesis should be https://www.metastasis-research.org/angiogenesis changed in main menu url call.

#6282017-1013 Modification - Donation slider: 
Our vision is to de-throne metastasis as a disease that steals lives by obtaining research results that translate to patients having an optimal quality of life while managing metastasis as a chronic condition, with the ultimate goal of achieving a cure. (an instead of a)

Membership slider:
The Benefits of Membership: Find out why you should join the MRS. (delete period and remove capitalization of first part)
Changed to: The benefits of membership: find out why you should join the MRS

Upcoming conferences slider:
The MRS organizes an international congress every two years to provide a venue dedicated to the exchange of information and furtherance of research into all aspects of metastasis.
(an instead of a and deleted the d from provided)

#6282017-1014 Enhancement - Current sentence on registration page:
The MRS invites Supporting Members to take a “Seat at the Bench” and lend their voices to the development of patient-centered metastasis research projects/events and/or grants. Supporting Members can choose to participate in one or both of our member directories:

Please change this to add a bit more:
The MRS invites Supporting Members to take a “Seat at the Bench” and lend their voices to the development of patient-centered metastasis research projects/events and/or grants. Supporting Members can choose to participate in one or both of our member directories listed below.  
(Note: Directory participation does not require payment for membership on the next screen to enable those facing financial hardship to still participate actively in progressing patient-centered metastasis research.  However, only paid members will receive Supporting Member benefits).


#6282017-1015 Bug - v_front_page_news changed to plain text format to match the rest of the blocks on front page.

#6282017-1016 Enhancement - Try to use rules to modify email messages when members role is expiring and members role has expired. Unfortunately, the rules were not mutally exclusive and resulted in both members role and all other roles were expiring or had expired. Two messages were sent out, so modified rules messages to reflect issue of old members role expiring or had expired. Placed a notification indicating to disregard the message if the role had either expired or was about to expire.

#6282017-1017 Enhancement - Journal access per Springer's request/instructions and modifications to allow MRS members to access the journals.