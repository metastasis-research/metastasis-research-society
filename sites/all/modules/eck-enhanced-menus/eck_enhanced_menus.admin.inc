<?php

include_once 'pluralize.inc';

function eck_enhanced_menus_content($form, $form_state, $type) {
  $form_state['type'] = $type;

  if (isset($form_state['values']['operation']) && $form_state['values']['operation'] == 'delete') {
    // return node_multiple_delete_confirm($form, $form_state, array_filter($form_state['values']['nodes']));
  }
  $form['filter'] = eck_enhanced_menus_filter_form($type);
  $form['#submit'][] = 'eck_enhanced_menus_filter_form_submit';
  $form['admin'] = eck_enhanced_menus_admin_entites($type);

  return $form;
}

function eck_enhanced_menus_filter_form($type) {
  $session = isset($_SESSION['eck_enhanced_menus_overview_filter']) ? $_SESSION['eck_enhanced_menus_overview_filter'] : array();
  $filters = eck_enhanced_menus_filters($type);

  $inflect = new Inflect();
  $type_i = $inflect->pluralize($type);

  $i = 0;
  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Show only ' . $type_i . ' where'),
    '#theme' => 'exposed_filters__node',
  );
  foreach ($session as $filter) {
    list($type, $value) = $filter;
    if ($type == 'term') {
      // Load term name from DB rather than search and parse options array.
      $value = module_invoke('taxonomy', 'term_load', $value);
      $value = $value->name;
    }
    elseif ($type == 'language') {
      $value = $value == LANGUAGE_NONE ? t('Language neutral') : module_invoke('locale', 'language_name', $value);
    }
    else {
      $value = $filters[$type]['options'][$value];
    }
    $t_args = array('%property' => $filters[$type]['title'], '%value' => $value);
    if ($i++) {
      $form['filters']['current'][] = array('#markup' => t('and where %property is %value', $t_args));
    }
    else {
      $form['filters']['current'][] = array('#markup' => t('where %property is %value', $t_args));
    }
    if (in_array($type, array('type', 'language'))) {
      // Remove the option if it is already being filtered on.
      unset($filters[$type]);
    }
  }

  // $form['filters']['status'] = array(
  //   '#type' => 'container',
  //   '#attributes' => array('class' => array('clearfix')),
  //   '#prefix' => ($i ? '<div class="additional-filters">' . t('and where') . '</div>' : ''),
  // );
  $form['filters']['status']['filters'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('filters')),
  );
  foreach ($filters as $key => $filter) {
    $form['filters']['status']['filters'][$key] = array(
      '#type' => 'select',
      '#options' => $filter['options'],
      '#title' => $filter['title'],
      '#default_value' => '[any]',
    );
  }

  $form['filters']['status']['actions'] = array(
    '#type' => 'actions',
    '#attributes' => array('class' => array('container-inline')),
  );
  if (!count($session)) {
    $form['filters']['status']['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Filter'),
    );
  }
  if (count($session)) {
    $form['filters']['status']['actions']['undo'] = array('#type' => 'submit', '#value' => t('Undo'));
    $form['filters']['status']['actions']['reset'] = array('#type' => 'submit', '#value' => t('Reset'));
  }

  drupal_add_js('misc/form.js');

  return $form;
}

function eck_enhanced_menus_filters($type) {
  $entity = entity_get_info($type);

  $options = array();
  foreach ($entity['bundles'] as $key => $value) {
    $options[$key] = $value['label'];
  }

  $filters['type'] = array(
    'title' => t('type'),
    'options' => array(
      '[any]' => t('any'),
    ) + $options,
  );

  // Language filter if there is a list of languages
  if ($languages = module_invoke('locale', 'language_list')) {
    $languages = array(LANGUAGE_NONE => t('Language neutral')) + $languages;
    $filters['language'] = array(
      'title' => t('language'),
      'options' => array(
        '[any]' => t('any'),
      ) + $languages,
    );
  }
  return $filters;
}

/**
 * Process result from node administration filter form.
 */
function eck_enhanced_menus_filter_form_submit($form, &$form_state) {
  $type = $form_state['type'];

  $filters = eck_enhanced_menus_filters($type);
  switch ($form_state['values']['op']) {
    case t('Filter'):
    case t('Refine'):
      // Apply every filter that has a choice selected other than 'any'.
      foreach ($filters as $filter => $options) {
        if (isset($form_state['values'][$filter]) && $form_state['values'][$filter] != '[any]') {
          // Flatten the options array to accommodate hierarchical/nested options.
          $flat_options = form_options_flatten($filters[$filter]['options']);
          // Only accept valid selections offered on the dropdown, block bad input.
          if (isset($flat_options[$form_state['values'][$filter]])) {
            $_SESSION['eck_enhanced_menus_overview_filter'][] = array($filter, $form_state['values'][$filter]);
          }
        }
      }
      break;
    case t('Undo'):
      array_pop($_SESSION['eck_enhanced_menus_overview_filter']);
      break;
    case t('Reset'):
      $_SESSION['eck_enhanced_menus_overview_filter'] = array();
      break;
  }
}

function eck_enhanced_menus_admin_entites($type) {
  $admin_access = user_access('administer ' . $type . ' entites');
  $parent_entity = entity_get_info($type);

  // Build the 'Update options' form.
  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update options'),
    '#attributes' => array('class' => array('container-inline')),
    '#access' => $admin_access,
  );
  $options = array();
  foreach (module_invoke_all('node_operations') as $operation => $array) {
    $options[$operation] = $array['label'];
  }
  $form['options']['operation'] = array(
    '#type' => 'select',
    '#title' => t('Operation'),
    '#title_display' => 'invisible',
    '#options' => $options,
    '#default_value' => 'approve',
  );
  $form['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
    '#validate' => array('node_admin_nodes_validate'),
    '#submit' => array('node_admin_nodes_submit'),
  );

  // Enable language column if translation module is enabled or if we have any
  // node with language.
  $multilanguage = (module_exists('translation') || db_query_range("SELECT 1 FROM {node} WHERE language <> :language", 0, 1, array(':language' => LANGUAGE_NONE))->fetchField());

  // Build the sortable table header.
  $header = array(
    'title' => array('data' => t('Title'), 'field' => 'n.title'),
    'type' => array('data' => t('Type'), 'field' => 'n.type'),
  );

  if (in_array('changed', $parent_entity['schema_fields_sql']['base table'])) {
    $header['changed'] = array('data' => t('Updated'), 'field' => 'n.changed', 'sort' => 'desc');
  }

  if ($multilanguage) {
    $header['language'] = array('data' => t('Language'), 'field' => 'n.language');
  }
  $header['operations'] = array('data' => t('Operations'));

  $entites = eck_enhanced_menus_build_filter_query($type);

  $languages = language_list();
  $destination = drupal_get_destination();
  $options = array();

  foreach ($entites as $entity) {

    $id = $entity->id;
    // dpm($entity);

    $bundle = $parent_entity['bundles'][$entity->type];

    $view = str_replace('%', $id, $bundle['crud']['view']['path']);
    $edit = str_replace('%', $id, $bundle['crud']['edit']['path']);
    $delete = str_replace('%', $id, $bundle['crud']['delete']['path']);

    $options[$entity->type . '-' . $entity->id] = array(
      'title' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => $entity->title,
          '#href' => $view,
        ),
      ),
      'type' => check_plain($bundle['label']),
    );

    if (!empty($entity->changed)) {
      $options[$entity->type . '-' . $entity->id]['changed'] = format_date($entity->changed, 'short');
    }


    // Build a list of all the accessible operations for the current node.
    $operations = array();
    if (user_access('eck edit ' . $type . ' ' . $entity->type . ' entities')) {
      $operations['edit'] = array(
        'title' => t('edit'),
        'href' => $edit,
        'query' => $destination,
      );
    }
    if (user_access('eck delete ' . $type . ' ' .  $entity->type . ' entities')) {
      $operations['delete'] = array(
        'title' => t('delete'),
        'href' => $delete,
        'query' => $destination,
      );
    }
    $options[$entity->type . '-' . $entity->id]['operations'] = array();
    if (count($operations) > 1) {
      // Render an unordered list of operations links.
      $options[$entity->type . '-' . $entity->id]['operations'] = array(
        'data' => array(
          '#theme' => 'links__node_operations',
          '#links' => $operations,
          '#attributes' => array('class' => array('links', 'inline')),
        ),
      );
    }
    elseif (!empty($operations)) {
      // Render the first and only operation as a link.
      $link = reset($operations);
      $options[$entity->type . '-' . $entity->id]['operations'] = array(
        'data' => array(
          '#type' => 'link',
          '#title' => $link['title'],
          '#href' => $link['href'],
          '#options' => array('query' => $link['query']),
        ),
      );
    }
  }

  if ($admin_access) {
    $form['entities'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => t('No content available.'),
    );
  }
  else {
    $form['entities'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $options,
      '#empty' => t('No entities available.')
    );
  }

  $form['pager'] = array('#markup' => theme('pager'));
  return $form;
}

/**
 * Apply filters for node administration filters based on session.
 *
 * @param $query
 *   A SelectQuery to which the filters should be applied.
 */

function eck_enhanced_menus_build_filter_query($type) {

  $filter_data = isset($_SESSION['eck_enhanced_menus_overview_filter']) ? $_SESSION['eck_enhanced_menus_overview_filter'] : array();

  if (!empty($filter_data)) {
    $bundle = check_plain($filter_data[0][1]);

    $query = new EntityFieldQuery;

    $result = $query
            ->entityCondition('entity_type', $type)
            ->entityCondition('bundle', $bundle)
            ->execute();

    $ids = array();
    foreach ($result[$type] as $entity) {
      $ids[] = $entity->id;
    }
    return entity_load($type, $ids);
  }
  else {
    return entity_load($type);
  }
}

/**
 * Decide on the type of marker to be displayed for a given node.
 *
 * @param $nid
 *   Node ID whose history supplies the "last viewed" timestamp.
 * @param $timestamp
 *   Time which is compared against node's "last viewed" timestamp.
 * @return
 *   One of the MARK constants.
 */
function eck_enhanced_menus_mark($entity, $type) {
  global $user;
  $cache = &drupal_static(__FUNCTION__, array());

  if (!$user->uid) {
    return MARK_READ;
  }
  dpm($entity);

  $key = $type . '-' . $entity->type;
  $id =  $entity->id;
  $uid = $key . '-' . $id;

  $timestamp = $entity->changed;

  if (!isset($cache[$uid])) {
    $cache[$uid] = eck_enhanced_menus_last_viewed($key, $id);
  }
  if ($cache[$uid] == 0 && $timestamp > NODE_NEW_LIMIT) {
    return MARK_NEW;
  }
  elseif ($timestamp > $cache[$uid] && $timestamp > NODE_NEW_LIMIT) {
    return MARK_UPDATED;
  }
  return MARK_READ;
}

/**
 * Retrieves the timestamp at which the current user last viewed the
 * specified node.
 */
function eck_enhanced_menus_last_viewed($key, $id) {
  global $user;
  $history = &drupal_static(__FUNCTION__, array());
  $uid = $key . '-' . $id;


  if (!isset($history[$uid])) {
    $time = db_select('eck_enhanced_menus_history', 'e')
          ->fields(e, array('timestamp'))
          ->condition('uid', $user->uid, '=')
          ->condition('eid', $id)
          ->condition('etype', $key)
          ->execute()
          ->fetchAssoc();

    $history[$uid] = $time;

  }

  return (isset($history[$uid]->timestamp) ? $history[$uid]->timestamp : 0);
}