(function($) {
  			$(document).ready(function(){
				//Examples of how to assign the Colorbox event to elements
        for (var i = 1; i <=10; i++) {
				  var $grp = $(".colorbox-group" + i);
          if ($grp.length) {
            $grp.colorbox({
              rel:'colorbox-group'+i,
              transition: $grp.hasClass('fade')? 'fade' : 'none',
              slideshow: $grp.hasClass('slideshow'),
            });
          }
        }
				$(".colorbox-video").colorbox({iframe:true, innerWidth:640, innerHeight:390});
				$(".colorbox-iframe").colorbox({iframe:true, width:"80%", height:"80%"});
				$(".colorbox-inline").colorbox({inline:true, width:"50%"});
				
			});

})(jQuery);
