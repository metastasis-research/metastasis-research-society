<?php
/**
 * @file
 * System module integration.
 */

/**
 * Implementation of hook_profiler_components() on behalf of system.
 */
function system_profiler_components() {
  return array(
    'theme' => array('callback' => 'profiler_install_theme'),
    'variables' => array('callback' => 'profiler_install_variables'),
  );
}

/**
 * Component install callback for 'theme'.
 */
function profiler_install_theme($theme_name, $config, &$identifiers) {
  theme_enable(array($theme_name));
  variable_set('theme_default', $theme_name);
}

/**
 * Component install callback for 'variables'.
 */
function profiler_install_variables($vars, $config, &$identifiers) {
  foreach($vars as $key => $value) {
    // Handle custom keys.
    if ($key == 'user_admin_role' && $rid = _profiler_role_id_from_name($value)) {
      $value = $rid;
    }
    variable_set($key, $value);
  }
}
