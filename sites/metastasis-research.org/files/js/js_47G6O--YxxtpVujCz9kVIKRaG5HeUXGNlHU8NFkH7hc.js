(function ($) {

Drupal.behaviors.commentNotify = {
  attach: function (context) {
    $('#edit-notify', context)
      .bind('change', function() {
        $('#edit-notify-type', context)
          [this.checked ? 'show' : 'hide']()
          .find('input[type=checkbox]:checked').attr('checked', 'checked');
      })
      .trigger('change');
  }
}

})(jQuery);
;
jQuery(function() {
  var $ = jQuery;
  
  if ($('#chart-container').length == 0) return;
  
  var cancerData;
  // data for bar graphs, indexed by graph and organ
  // eg barGraphsData['Stage']['Melanoma'] = [{name:..., y: ...}....]
  var barGraphsData = {
    "Stage"  : {},
    "5yr"    : {},
    "Pattern": {}
  };

  $.get(Drupal.settings.mrs_custom.chart_data_path, function(data) {

    var datatype = typeof(data);
    if(datatype != 'object'){
      cancerData = eval("(" + data + ')');
    }else{
      cancerData = data;
    }

    for (var i = 0; i < cancerData.length; i++){
      var item = cancerData[i];
      var organ = item['Organ'];
      barGraphsData['Stage'  ][organ] = [];
      barGraphsData['5yr'    ][organ] = [];
      barGraphsData['Pattern'][organ] = [];
      for (var key in item) {
        if ('Pattern: Total' == key) continue;
        // if ('Pattern: # patients' == key) continue;
        
        // key is something like "Stage: In Situ"
        var                       chartTypeSeriesName = key.split(': ');
        var chartType           = chartTypeSeriesName[0];
        var chartSeriesName     = chartTypeSeriesName[1];
        
        // console.log(key, chartTypeSeriesName);
        if (! chartSeriesName ) continue;
        // console.log(item[key]);
        if (Number(item[key]) > 0) {
          barGraphsData[chartType][organ].push({
            name: chartSeriesName,
               y: Number(item[key])
          });
        }
      }
    }
    
    // console.log(barGraphsData);
    

    renderChart($('input:checked[name=chart_type]').val() || 'Estimated New Cases');
  });
  
  $('[name=chart_type]').click(function() {
    renderChart(this.value)
  });
  
  function renderChart(totalsColumn){
    var total = 0;
    var systems = {};
    var organs = [];
    
    for (var i = 0; i < cancerData.length; i++){
      var item = cancerData[i];
      var organSystem = item['Organ System'];
      if (!systems[organSystem]) {
        systems[organSystem] = {
           name: organSystem,
              y: 0,
          color: item['system color']
        };
      }
      systems[organSystem].y += Number(item[totalsColumn]);
      total +=  Number(item[totalsColumn]);
      
      organs.push ({
         name: item['Organ'],
            y: item[totalsColumn],
        color: item['color'],
          cat: organSystem
      });
    }
    
    var series1 = []
    for (var i in systems) {
      systems[i].y  = systems[i].y * 100 / total;
      series1.push(systems[i]);
    }
    
    for (var i = 0; i < cancerData.length; i++) {
      organs[i].y  = organs[i].y * 100 / total;
    }
    
    var chart = $('#chart-container').highcharts();
    //chart.setTitle(totalsColumn);
    chart.series[0].setData(series1,false);
    chart.series[1].setData(organs,false);
    chart.redraw();
  }
  
  
  
  // Create the chart
  $('#chart-container').highcharts({
      chart: {
          type: 'pie',
          backgroundColor: '#faf9ed',
          borderWidth: 0,
          width: 750,
          height: 600,
      },
      credits: {
          enabled: false
      },
      labels: {
          style: {
              color: '#000000'
          }
      },
      title: {
          text: 'Sites of Cancer',
      },
      yAxis: {
          title: {
              text: 'Percent all cancer patients'
          }
      },
      plotOptions: {
          pie: {
              shadow: false,
              center: ['50%', '50%'],
              cursor: 'pointer',
              events: {
                click: seriesClicked
              },
              states: {
                hover: {
                  enabled: true
                }
              }
          }
      },
      tooltip: {
              formatter: function() {
                  // display only if larger than 1
                  var tip = '';
                  if (this.point.cat) {
                    tip = this.point.cat + '<br>';
                  }
             
                  tip += '<b>'+ this.point.name +':</b> '+ this.y.toFixed(1) +'%';
                  
                  return tip;
              },
              positioner: function(boxWidth, boxHeight, point){
              	
                return {x: 660-boxWidth, y: 105};
              }
      },
      series: [{
          name: 'Population',
          data: [],
          size: '40%',
          innerSize: '0%',
          dataLabels: {
              // formatter: function() {
                  // return this.y > 15 ? this.point.name : null;
              // },
              // color: 'white',
              // distance: -30
              enabled: false
          }
      }, {
          name: 'Population',
          data: [],
          size: '50%',
          innerSize: '40%',
          dataLabels: {
              formatter: function() {
                  // display only if larger than 1
                  return this.y > 2 ? '<b>'+ this.point.name +':</b> '+ this.y.toFixed(1) +'%'  : null;
              }
          }
      }]
  });
  
  
  function seriesClicked(event) {;
    // console.log(this, event.point);
    var point = event.point;
    var organ = point.name;
    
    var chartsToShow = [];

    for (var chartType in barGraphsData) {
      var data = barGraphsData[chartType][organ];
      var $container = $('#c' + chartType + 'Chart-container');


      // console.log(chartType);
      if (! data || (data.length == 0) ) {
        $container.fadeOut();
        //$("#chart-container").animate({'margin-left': '-18px'}, "slow");
        continue;   
      }   

      var chart = $container.highcharts();
      chart.series[0].setData(data, false);
      
      var categories = [];
      for (var i =0; i < data.length; i ++) {
        categories.push(data[i].name);
      }
      // console.log(chart.xAxis);
      chart.xAxis[0].setCategories(categories);
      chartsToShow.push($container);
      // console.log($container);
      // if ($("#chart-container").css('margin-left') != "-239px") {
      //$container.fadeIn();

      // }
      // else {$container.show();}
    }
    // if (chartsToShow.length > 0) {
      // // console.log("moving left");
      // $("#chart-container").animate({'margin-left': '-239px'}, "slow");
    // } else {
      // // console.log("moving back right");
      // $("#chart-container").animate({'margin-left': '-18px'}, "slow");
    // }
    $('#sub-chart-container').show();
    $.each(chartsToShow, function () {
      this.fadeIn();
    });
  }
  
  // instantiate subchart
  $('#cStageChart-container').highcharts({
        chart: {
            type: 'column',
            backgroundColor: '#faf9ed',
            plotBackgroundColor: '#faf9ed',
            width: 180,
            height: 175
//                marginLeft: -100px
        },
        credits: {
            enabled: false
        },
        legend: {
            enabled: false
        },
        title: {
            text: 'Stage at Diagnosis',
            align: 'center'
        },
        xAxis: {
            labels: {
              align: 'right',
              rotation: -45
            }
        },
        yAxis: {
            min: 0,
            max: 100,
            tickInterval: 25,
            title: {
              text: "Percentage (%)"
            }
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        tooltip: {
          formatter: function() {
            // display only if larger than 1
            var tip = '';
            if (this.point.cat) {
              tip = this.point.cat + '<br>';
            }
       
            tip += '<b>'+ this.point.name +':</b> '+ this.y.toFixed(1);
            
            return tip;
          }
        },
        series: [{}]
    });

    // instantiate subchart
  $('#c5yrChart-container').highcharts({
        chart: {
            type: 'column',
            backgroundColor: '#faf9ed',
            plotBackgroundColor: '#faf9ed',
            width: 180,
            height: 175
//                marginLeft: -100px
        },
        credits: {
            enabled: false
        },
        legend: {
            enabled: false
        },
        title: {
            text: '5 Year Survival',
            align: 'center'
        },
        xAxis: {
            labels: {
              align: 'right',
              rotation: -45
            }
        },
        yAxis: {
            min: 0,
            max: 100,
            tickInterval: 25,            
            title: {
              text: "Percentage (%)"
            }
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        tooltip: {
          formatter: function() {
            // display only if larger than 1
            var tip = '';
            if (this.point.cat) {
              tip = this.point.cat + '<br>';
            }
       
            tip += '<b>'+ this.point.name +':</b> '+ this.y.toFixed(1);
            
            return tip;
          }
        },
        series: [{}]
    });
    
      // instantiate subchart
  $('#cPatternChart-container').highcharts({
        chart: {
            type: 'column',
            backgroundColor: '#faf9ed', //#faf9ed or #faf9ed
            plotBackgroundColor: '#faf9ed',
            width: 380,
            height: 200
        },
        credits: {
            enabled: false
        },
        legend: {
            enabled: false
        },
        title: {
            text: 'Pattern of Metastasis',
            align: 'center'
        },
        xAxis: {
            labels: {
              align: 'right',
              rotation: -60,
              style: {
                fontSize: 5
              }
            }
        },
        yAxis: {
            min: 0,
            tickInterval: 100,     
            title: {
              text: "# Metastases"
            }
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        tooltip: {
          formatter: function() {
            // display only if larger than 1
            var tip = '';
            if (this.point.cat) {
              tip = this.point.cat + '<br>';
            }
       
            tip += '<b>'+ this.point.name +':</b> '+ this.y.toFixed(1);
            
            return tip;
          }
        },
        series: [{}]
    });

    //sites[i].color =  Highcharts.Color(catColours[sites[i].cat]).brighten(0.1).get();
    
  $('.closebox').click(function(){
    $('#sub-chart-container').hide();
  });
  
});
;
